package com.investree.demo.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;


import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
@Table(name = "payment_history")
public class PaymentHistory implements Serializable {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonIgnore
    @OneToMany(mappedBy = "payment_history", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Transaksi> transaksi;

    @Column(name = "pembayaran_ke", nullable = false, length = 11)
    private int pembayaran_ke;

    @Column(name = "jumlah", nullable = false, length = 11)
    private Double jumlah;

    @Column(name = "bukti_pembayaran", nullable = false, length = 11)
    private String bukti_pembayaran;

}
